﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;

namespace URLRedirector.Controllers
{
    [Route("")]
    public class RedirectController : Controller
    {
        /// <summary>
        /// Ex.: "veja.io/aH6Tx"
        /// Identifica o usuário pelo path da URL.
        /// </summary>
        /// <param name="path"></param>
        [HttpGet("{path}")]
        public void Get(string path)
        {
            const string SQL_CLICK = @"
				DECLARE @id_url INT
				DECLARE @redirect VARCHAR(200)

                SELECT TOP 1
                    @id_url = urls.id, @redirect = campanhas.url_destino
                FROM urls
				JOIN campanhas ON campanhas.id = urls.id_campanha
                WHERE
                    path = @path				

                INSERT INTO clicks
                    (id_url, ip, user_agent)
                VALUES
                    (@id_url, @ip, @user_agent)
					
                SELECT
                    @redirect
            ";

            string redirectURL;

            using (SqlConnection conn = new SqlConnection(Startup.ConnectionString))
            {
                conn.Open();

                using (SqlCommand comm = new SqlCommand(SQL_CLICK, conn))
                {
                    comm.Parameters.AddWithValue("@path", Request.Path.Value.Substring(1));
                    comm.Parameters.AddWithValue("@ip", Request.HttpContext.Connection.RemoteIpAddress.ToString());
                    comm.Parameters.AddWithValue("@user_agent", Request.Headers["User-Agent"].ToString());
                    redirectURL = comm.ExecuteScalar().ToString();
                }
            }

            Response.Redirect(redirectURL);
        }
    }
}