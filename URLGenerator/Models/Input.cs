﻿namespace URLGenerator.Models
{
    public class Input
    {
        public int IdCampanha { get; set; }
        public string[] Destinatarios { get; set; }
    }
}