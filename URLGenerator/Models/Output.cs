﻿namespace URLGenerator.Models
{
    public class Output
    {
        public string Destinatario { get; set; }
        public string URL { get; set; }
    }
}
