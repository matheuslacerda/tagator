﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using URLGenerator.Models;
using System.Data.SqlClient;
using System.Data;

namespace URLGenerator.Controllers
{
    [Route("api/GetURL")]
    public class GetURLController : Controller
    {
        [HttpPost]
        public IActionResult Post([FromBody]Input input)
        {
            const string SQL_SELECT_DOMAIN = @"
                SELECT TOP 1
                    domains.domain
                FROM
                    domains
                    JOIN campanhas
                        ON campanhas.id_domain = domains.id
                WHERE
                    campanhas.id = @id_campanha
            ";
            // TODO autenticação

            if (input == null)
                return StatusCode(400);

            List<Output> outputs = new List<Output>();

            using (SqlConnection conn = new SqlConnection(Startup.ConnectionString))
            {
                conn.Open();

                string domain;

                using (SqlCommand comm = new SqlCommand(SQL_SELECT_DOMAIN, conn))
                {
                    comm.Parameters.AddWithValue("@id_campanha", input.IdCampanha);
                    domain = comm.ExecuteScalar().ToString();
                }

                foreach (string destinatario in input.Destinatarios)
                {
                    using (SqlCommand comm = new SqlCommand(@"insert_url", conn))
                    {
                        comm.CommandType = CommandType.StoredProcedure;
                        comm.Parameters.AddWithValue("@id_campanha", input.IdCampanha);
                        comm.Parameters.AddWithValue("@destinatario", destinatario);
                        string path = comm.ExecuteScalar().ToString();

                        outputs.Add(new Output { Destinatario = destinatario, URL = domain + "/" + path });
                    }
                }
            }

            return Ok(outputs);
        }
    }
}